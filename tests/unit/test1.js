

describe("Slackie Tests", function() {


    describe("Search Tests", function() {



        describe("Google API", function() {
            describe("Base", function() {
                it('When I pass something other than as object to options, test should fail - pass number', function() {
                    //var stub = sinon.stub().throws("need to pass in object!");
                    assert.isDefined(Google, "Base Google API object not found");
                    var google = new Google(123);
                    assert.property(google, 'searchId');
                    expect(google.options).to.be.empty;

                })
                it('When I pass something other than as object to options, test should fail - pass boolean', function() {
                    assert.isDefined(Google, "Base Google API object not found");
                    var google = new Google(true);
                    assert.property(google, 'searchId');
                    expect(google.options).to.be.empty;
                })
                it('When I pass something other than as object to options, test should fail - pass null', function() {
                    assert.isDefined(Google, "Base Google API object not found");
                    var google = new Google(null);
                    assert.property(google, 'searchId');
                    expect(google.options).to.be.empty;
                })
                it('When I pass an object, test should pass - pass object', function() {
                    var google = new Google({});
                    expect(google).property('searchId', 'google');
                });
                it('When I pass nothing, Google prototype should still be ammended with options', function() {
                    var google = new Google();
                    expect(google).property('options');
                    expect(google.options).to.be.empty;
                });


            });
            describe('Google API - Image API - Inheritance from Google Base', function() {
                it('Image Search Object should always generate options even if Google object has not been modified with global options ', function() {
                    var googleImage = new GoogleImageSearch();
                    expect(googleImage).property('options');
                })
                it('Image Search Object should always generate options even if Google object has not been modified with global options ', function() {
                    var googleImage = new GoogleImageSearch(null);
                    expect(googleImage).property('options');
                })
                it('Image Search should always have default property: "searchType" with value: "image"', function() {
                    var googleImage = new GoogleImageSearch();
                    assert.deepPropertyVal(googleImage, 'options.searchType', "image");
                    //also can manually add on options
                    googleImage.options.added = "added";
                    assert.deepPropertyVal(googleImage, 'options.added', "added");

                })
                it('Image Search Object should inherit settings/options from Google Base', function() {
                    var google = new Google({ cx: "testcx", key: "testkey" });
                    var googleImage = new GoogleImageSearch({});
                    assert.deepPropertyVal(googleImage, 'options.cx', "testcx");
                    assert.deepPropertyVal(googleImage, 'options.key', "testkey");
                    assert.deepPropertyVal(googleImage, 'options.searchType', "image")
                    var googleImage = new GoogleImageSearch({ testoption: 'testOption' });
                    assert.deepPropertyVal(googleImage, 'options.cx', "testcx");
                    assert.deepPropertyVal(googleImage, 'options.key', "testkey");
                    assert.deepPropertyVal(googleImage, 'options.searchType', "image");
                    assert.deepPropertyVal(googleImage, 'options.testoption', "testOption");

                })
                it('Image Search Object should inherit settings/options from Google Base, change Base, change Inheritance by new()', function() {
                    var google = new Google({ cx: "testcx", key: "testkey" });
                    var googleImage = new GoogleImageSearch({});
                    assert.deepPropertyVal(googleImage, 'options.cx', "testcx");
                    assert.deepPropertyVal(googleImage, 'options.key', "testkey");
                    assert.deepPropertyVal(googleImage, 'options.searchType', "image")
                    google.options.addon = "addon";
                    assert.deepPropertyVal(google, 'options.addon', "addon");
                    assert.notDeepProperty(googleImage, 'options.addon', "addon");
                    var googleImage = new Google(google.options);
                    assert.deepPropertyVal(google, 'options.addon', "addon");
                    assert.deepPropertyVal(googleImage, 'options.addon', "addon");
                })

                describe('Google API - Image API - Internal Function Testing - Missing Parameters', function() {
                    it('Function - getQuery : should return string with missing either cx or key options', function() {
                        new Google();
                        var googleImage = new GoogleImageSearch();
                        assert.equal(googleImage.getQuery("dogs"), null);
                        googleImage = new GoogleImageSearch({key : "test"});
                        assert.equal(googleImage.getQuery("dogs"), null);
                        googleImage = new GoogleImageSearch({cx : "test"});
                        assert.equal(googleImage.getQuery("dogs"), null);
                        googleImage = new GoogleImageSearch({cx : "test", key: "test"});
                        assert.isString(googleImage.getQuery("dogs"));
                        //only wipe out options of Google
                        new Google();
                        assert.isString(googleImage.getQuery("dogs"));
                    })
                    it('Google API - Image API - Internal Function Testing - getQuery should store to lastQueryUrl', function() {

						var google = new Google({
                            cx: "017150484625148396984%3Au7dyopvvmpe",
                            key: "AIzaSyC5QtIt6UFARoHHQKe9zFnPe-cq5zW4Enk"
                        });

                        assert.deepPropertyVal(google, 'options.cx', "017150484625148396984%3Au7dyopvvmpe");

                        var googleImage = new GoogleImageSearch();
                        assert.deepPropertyVal(googleImage, 'options.cx', "017150484625148396984%3Au7dyopvvmpe");

                        //store a url that is returned for query: 'dogs'
                        var _url = googleImage.getQuery("dogs");
                        //console.log("url returned from query is" + _url);

                        //make sure it returns a string
                        assert.isString(_url);
                        assert.equal(googleImage.getQuery("dogs"), _url);
                        assert.notEqual(googleImage.getQuery("dogsAgain"), _url, "it does match");
                    })

                })

            });

        });

    })

});
