
	
#SlackieSearch

![image](http://seansresu.me/slackie/img/slackieHappy.gif)
### Intro

Hi there!  My name is Sean Finkel and this is my coding challenge for Slack!  
######Thank you for this opportunity! 

I wanted to give you an overview of my approach for the challenge and explain why went the way I did.

To start off, I wanted to:

* Make it able to use multiple image search APIs.  
* Make the code modular so it could be expanded upon.
* Make it creative. I created a character called 'Slackie'.  I hope this was okay (if wished, I will remove him), but Slackie definitely adds character? (no pun intended) to the project ha.
* Make it pretty, because after all, this is frontend ha.

Logically, I am sure things could be done much differently and I look forward to hearing those suggestions. For instance, one of the things I debated for a while was whether or not to store the images in to an array as responses came in and have the thumbnail -> lightbox transition managed that way. I decided against it, with my reasoning being, I didn't want to load up a variable with a whole bunch of objects as they come in. I decided instead to have the src stored in the DOM on the DIV that held the <img> from the response and when the thumbnail was clicked, would send it's url in to the lightbox image for loading. I also ended up liking this approach as the browser cache started doing a lot of heavy lifting ha (at least for Mac: Chrome, Safari, Firefox & Windows: ie11). The result was that I found switching images was pretty quick.


Lastly, I put a ton of tooling around the project. I wanted to give it the full treatment of unit testing, release building, etc. so I hope to be able to finish that.

#####Anwyay, I hope you enjoy the project!

###Thanks again for the opportunity!

#####(PS: Please see 'SlackieDesign.pdf' for more about technical details of design)


### Version
0.0.1

### Tech Involved

* [node.js] - evented I/O for the backend
* [Gulp] - the streaming build system (and many gulp plugins! ..see: package.json)
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [sass] - Syntactically Awesome Style Sheets
* [jsdoc] - JavaScript documentation generator
* [jshint] - a JavaScript Code Quality Tool
* [mocha] - the fun, simple, flexible JavaScript test framework
* [karma] - Spectacular Test Runner for Javascript
* [gitlab] - Git repository management
* []phantomjs] - Headless WebKit scriptable with a JavaScript API

### Installation (if you want to make a build)

In order to make a build, you need Node.JS & Gulp installed globally. Gulp is an awesome build tool that made my life way easier when developing this project.

Here's how to install Gulp:

```
$ npm i -g gulp
```
With the global '-g' flag, you may need to run as 'sudo' instead:

```
$ sudo npm i -g gulp
```

Likewise, install mocha:

```
npm install -g mocha
```

karma:

```
npm install -g karma
```

and jsodc:

```
npm install -g jsdoc
```

You may also need to install 'phantomJS' for karma.
 
Finally, install all npm dependencies:

```
npm install
```


### Trying it out (if you don't want to make a build)

I have put a development build all wrapped up in a src.zip file at this location:

```
$ ./build/dev/src.zip
```

Just unzip the file, go in and open the slackie.html file in a browser.  It should be good to go.


### Building the 'fun' way

If you want to try out the development stuff, here are some commands I use regularly.  First, in a terminal, change to the directory and use one of these commands:

##### Development

```
gulp build-dev
```

* A new development build is created


```
gulp watch-dev
```

* Watches the /src folder for changes
* When a file is changed, a new development build is created
* A page load is triggered
* If this is the first time the command is run, it will try to open the build in the browser


##### Release

```
gulp release-dev
```

* A new release build is created


```
gulp build-release
```
* Watches the /src folder for changes
* When a file is changed, a new RELEASE build is created
* A page load is triggered
* If this is the first time the command is run, it will try to open the build in the browser



This should generate a build based on your current /src files any time a change is made or even if they are saved.  If 'liveReload' is enabled in the settings.js file, and the gulp watch-dev is still running in a terminal, then you should see the page reload everytime a change is made as well.

This way, you know if you are failing (which is pretty common with me haha), and you know if you messed up something.  If you have a JS error, you may need to reload the page depending on what level of the code, the error happens.  Majority of the time it will reload.

So like previously mentioned, when you save a file, a build will be placed in ./build/dev/src/.  On the first time the command is run, Gulp will try to open your web browser (or open a new tab) with the build.  After that, the page will use livereload to show live changes.

**CSS/SASS:** Gulp compiles the sass and css files and concatenates them in to one for the final build.  An autoprefixer is run on the files to make sure I didn't leave out vendor prefixes.

**JS**: Gulp takes all the JS right now and concatenates it in to one file.  It also brings in the Mock data (used so we don't have to query Google all the time), which makes the file a litle bigger.  I would normally set up a release task to ignore bringing these files in, but I am running out of time.






   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [Gulp]: <http://gulpjs.com>
   [Sass]: <http://sass-lang.com/>
   [gulp-autoprefixer]: <https://www.npmjs.com/package/gulp-autoprefixer>
   [gulp-livereload]: <https://www.npmjs.com/package/gulp-livereload>
   [gulp-html-replace]: <https://www.npmjs.com/package/gulp-html-replace>
   [gulp-sass]: <https://www.npmjs.com/package/gulp-sass>
   [jsdoc]: <http://usejsdoc.org/>
   [jshint]: <http://jshint.com/>
   [mocha]: <https://mochajs.org/>
   [karma]: <https://karma-runner.github.io/>
   [gitlab]: <https://gitlab.com/>
   [phantomjs]: <phantomjs.org>
  





