var settings = function() {
    return {
        debug: true,
        fakeData: false,  //no live api calls, good for when queries cost you $ ha
        liveReload: false,
        google: {
            image: {},
            options: {
                cx: "017150484625148396984%3Au7dyopvvmpe",
                key: "AIzaSyC5QtIt6UFARoHHQKe9zFnPe-cq5zW4Enk"
                    //another key in case:
                    // cx: "013808203020209060620%cpbhbpkxfaq",
                    // key: "AIzaSyA-p66OC0iEgseek9bT6wAUJoYTBYLgUAQ"
            },
        },
        flikr: {
            options: {
                method: "flickr.photos.search",
                api_key: "3ae8ff4bba534d7d137274941f6c09c8",
                per_page: "10",
            }
        },
        imgur: {
            requestHeader: { "authorization": "Client-ID e6da99e8c3c54a9" },
            options: {
                q_type: 'anigif' //seems like you can only do one type?  png, gif, jpg, anigif ... =(
            }
        },
        giphy: {
            image: {
                limit: 10
            },
            options: {
                api_key: "dc6zaTOxFJmzC",
            }
        },
        gui: {
            automaticSearch: false,
            bumpers: true, //in lightbox if you hit the beginning or end, BUMP!
            delayImageLoad: 0, //in ms
            fadeInLightbox: true, //if you want the lightbox to fade out
            fadeOutLightbox: true, //if you want the lightbox to fade in
            infiniteScroll: true, //keep scrollin' on
            infiniteScrollTimes: 3, //how many times you can infinite scroll before gettings stopped (not-so-infinite-scroll?)
            delayResponse: false, //in ms  //master of the delays
            delayGoogleResponse: 1000, //in ms
            delayFlikrResponse: 5000, //in ms
            delayImgurResponse: 5000, //in ms
            delayGiphyResponse: 5000, //in ms
        }
    }
}()