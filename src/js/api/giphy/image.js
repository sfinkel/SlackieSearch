'use strict';
/*global console, settings, ajax, mockGiphyImageResponse*/
// Define the Giphy constructor
var Giphy = function(p_options) {
    if (this.constructor === Giphy) {
        if (p_options && (typeof p_options === 'object')) {
            this.add(p_options);
        } else {
            this.add({});
        }
    } else {
        if (p_options) {
            for (var attrname in Giphy.prototype.options) { p_options[attrname] = Giphy.prototype.options[attrname]; }
        }
    }

    this.searchId = "giphy";
};

//Add global options to Giphy prototype if global options are found 'settings.giphy.options'
Giphy.prototype.add = function(p_options) {
    Giphy.prototype.options = p_options;
};

//Giphy.prototype.options = settings.giphy.options || {};
Giphy.prototype.constructor = Giphy;


// Define the GiphyImageSearch constructor (inherits options from Giphy)
function GiphyImageSearch(options) {
    //this.options = options || {};
    // Call the parent constructor, making sure (using Function#call)
    // that "this" is set correctly during the call

    //store the passed options
    if (options) {
        this.options = options;
    }

    //Call the parent constructor with passed in options
    Giphy.call(this, options);

    //this.options here --v  now have been modified by Giphy ^

    this.options = this.options || {};

    //this.options should be ammended / merged with inherited global options


    //override options passed in with Giphy global options

    //Following properties specific to Giphy Image Search API
    //override the name inherited from the generic "giphy"
    this.searchId = "giphyImage";

    this.options.offset = 0;
    this.lastMethod = "";
    this.lastQuery = "";
    this.lastUserCallback = "";
    this.lastQueryUrl = "";
    this.lastValidResponseQueryUrl = "";

    //initializes the function back to its starting state
    this.init = function() {
        this.options.offset = 0;
        this.lastMethod = "";
        this.lastQuery = "";
        this.lastUserCallback = "";
        this.lastQueryUrl = "";
        this.lastValidResponseQueryUrl = "";
    };

    //this returns the actual query string that we will use to send
    this.getQuery = function(p_query) {
        if (!this.options.api_key || !p_query) {
            //console.log("need propery query params! DEBUG: searchType: " + this.options.searchType + " cx: " + this.options.cx + " key: " + this.options.key + " query: " + p_query + "");
            this.init();
            return null;
        }
        var queryUrl = 'http://api.giphy.com/v1/gifs/search?';
        this.options.q = encodeURIComponent(p_query).replace(/%20/g, "+");
        for (var i in this.options) {
            queryUrl += '&' + i + '=' + this.options[i] + '';
        }

        this.lastQuery = p_query;
        return queryUrl;
    };

    //This function extracts the start point for queryNext()..i.e. image 11 of results
    this.storeNextStartIndex = function(p_jsonObject) {
        //borrow the json to extract next page number
        try {
            //console.log(p_jsonObject)
            if((p_jsonObject.data.length + this.options.offset) > this.options.offset)
            {
                this.options.offset += p_jsonObject.data.length;
            }
        } catch (err) {
            console.log("error with decode" + err);
        }

    };



    //This does much of the heavy lifting, sends off the ajax request and parses the results
    //send to the user's callback function for processing the result object
    this.sendQuery = function(p_method, p_queryUrl, p_userCallback) {
        if (!p_method || !p_queryUrl || !p_userCallback) {
            //console.log("need proper params: p_method: " + p_method + " p_queryUrl");
            return null;
        }

        this.lastQueryUrl = p_queryUrl;
        this.lastMethod = p_method;
        this.lastUserCallback = p_userCallback;

        //will need referrence to the scope of GiphyImageSearch for this.options.start
        var self = this;

        //ajax success function and interception
        function jsonSuccess(p_jsonData) {
            var _jsonObject = null;
            try {
             _jsonObject = JSON.parse(p_jsonData);
            } catch (err) { console.log("json didn't parse: " + err); }

            //borrow the json to extract next page number
            self.storeNextStartIndex(_jsonObject);

            //value saved, now call user's actual passed function
            p_userCallback(_jsonObject);

            //for testing
            self.lastValidResponseQueryUrl = p_queryUrl;
        }

        //ajax failure =(
        function jsonFail(p_failText) {
            console.log("JSON failed =(  ..Did you run out of daily queries?" + p_failText);
        }
        if (settings.fakeData) {
             jsonSuccess(JSON.stringify(mockGiphyImageResponse()));

        } else {
            //custom ajax request function with optional success/failure function
            ajax.request(p_method, p_queryUrl, jsonSuccess, jsonFail);
        }
    };

    //forms the query and used the passed GET/METHOD with a success callback
    this.query = function(p_method, p_query, p_userCallback) {
        //check if queried before, if not do same with previous?
        this.init();
        this.sendQuery(p_method, this.getQuery(p_query), p_userCallback);
    };
    //forms the query and used the passed GET/METHOD with a success callback
    this.queryNext = function() {
        if (!this.lastMethod || !this.lastQuery || !this.lastUserCallback) {
            console.log("have to query first!");
            this.init();
            return;
        }
        this.sendQuery(this.lastMethod, this.getQuery(this.lastQuery), this.lastUserCallback);

    };
}

//Tell GiphyImageSearch to Inherit from Giphy prototype
GiphyImageSearch.prototype = Object.create(Giphy.prototype); // See note below

// Set the "constructor" property to refer to GiphyImageSearch
GiphyImageSearch.prototype.constructor = GiphyImageSearch;