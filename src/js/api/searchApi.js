'use strict';
/*global console  */
var Search = function() { // jshint ignore:line

    //lastQuery stores the last search string
    this.lastQuery = null;

    //searches is an object that holds the attached APIs.
    this.searches = {};

    var init = function(){
        for (var search in this.searches) {
            this.searches[search].init();
        }
    };

    //function to do a fresh query and reset all child APIs
    var query = function(p_query) {
        init();
        for (var search in this.searches) {
            if (this.searches[search].activated) {
                //console.log('INITIAL RESUTLTS with SearchId: ' + this.searches[search].searchId);
                this.searches[search].query(this.searches[search].method, p_query, this.searches[search].callback);
            }
        }
        this.lastQuery = p_query;
    };

    //function to use all child APIs queryNext function to do a query
    var queryNext = function() {
        for (var search in this.searches) {
            //console.log('NEXT RESUTLTS with SearchId: ' + this.searches[search].searchId);
            if (this.searches[search].activated) {
                this.searches[search].queryNext();
            }
        }
    };

    //function to attach an API to this object
    var add = function(p_search, p_method, p_callback) {
        //console.log(p_search.searchId);
        this.searches[p_search.searchId] = p_search;
        this.searches[p_search.searchId].method = p_method;
        this.searches[p_search.searchId].callback = p_callback;
        this.searches[p_search.searchId].activated = true;
    };


    var deactivate = function(p_searchId) {
        this.searches[p_searchId].activated = false;
    };

    var reactivate = function(p_searchId) {
        this.searches[p_searchId].activated = true;
    };

    var getActivatedSearches = function() {
        var _arr = [];
        for (var search in this.searches) {
            if (this.searches[search].activated) {
                _arr.push(this.searches[search].searchId);
            }

        }
        return _arr;
    };
    return {
        init: init,
        query: query,
        queryNext: queryNext,
        add: add,
        deactivate: deactivate,
        reactivate: reactivate,
        searches: this.searches,
        lastQuery : this.lastQuery,
        getActivatedSearches: getActivatedSearches,

    };
}();