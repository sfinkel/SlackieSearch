'use strict';
/*global console, settings, ajax, mockImgurImageResponse*/
// Define the Imgur constructor
var Imgur = function(p_options) {
    if (this.constructor === Imgur) {
        if (p_options && (typeof p_options === 'object')) {
            this.add(p_options);
        } else {
            this.add({});
        }
    } else {
        if (p_options) {
            for (var attrname in Imgur.prototype.options) { p_options[attrname] = Imgur.prototype.options[attrname]; }
        }
    }

    this.searchId = "imgur";
};

//Add global options to Imgur prototype if global options are found 'settings.imgur.options'
Imgur.prototype.add = function(p_options) {
    Imgur.prototype.options = p_options;
};

//Imgur.prototype.options = settings.imgur.options || {};
Imgur.prototype.constructor = Imgur;


// Define the ImgurImageSearch constructor (inherits options from Imgur)
function ImgurImageSearch(options) {
    //this.options = options || {};
    // Call the parent constructor, making sure (using Function#call)
    // that "this" is set correctly during the call

    //store the passed options
    if (options) {
        this.options = options;
    }

    //Call the parent constructor with passed in options
    Imgur.call(this, options);

    //this.options here --v  now have been modified by Imgur ^

    this.options = this.options || {};

    //this.options should be ammended / merged with inherited global options


    //override options passed in with Imgur global options

    //Following properties specific to Imgur Image Search API
    //override the name inherited from the generic "imgur"
    this.searchId = "imgurImage";


    this.start = 1;
    this.lastMethod = "";
    this.lastQuery = "";
    this.lastUserCallback = "";
    this.lastQueryUrl = "";
    this.lastValidResponseQueryUrl = "";

    //initializes the function back to its starting state
    this.init = function() {
        this.start = 1;
        this.lastMethod = "";
        this.lastQuery = "";
        this.lastUserCallback = "";
        this.lastQueryUrl = "";
        this.lastValidResponseQueryUrl = "";
    };

    //this returns the actual query string that we will use to send
    this.getQuery = function(p_query) {
        if (!p_query) {
            //console.log("need propery query params! DEBUG: :  cx: " + this.options.cx + " key: " + this.options.key + " query: " + p_query + "");
            this.init();
            return null;
        }
        var queryUrl = 'https://api.imgur.com/3/gallery/search/top/all/'+this.start+'?';

        this.options.q_all = encodeURIComponent(p_query).replace(/%20/g, "+");
        for (var i in this.options) {
            queryUrl += '&' + i + '=' + this.options[i] + '';
        }

        this.lastQuery = p_query;
        return queryUrl;
    };

    //This function extracts the start point for queryNext()..i.e. page++
    this.storeNextStartIndex = function(p_jsonObject) {
        //Doesn't look like imgur let's us know if there is another page of results or not
        if(!p_jsonObject){
            this.start = 1;
        }
        else{
            this.start++;
        }

    };

    //This does much of the heavy lifting, sends off the ajax request and parses the results
    //send to the user's callback function for processing the result object
    this.sendQuery = function(p_method, p_queryUrl, p_userCallback) {
        if (!p_method || !p_queryUrl || !p_userCallback) {
            //console.log("need proper params: p_method: " + p_method + " p_queryUrl");
            return null;
        }

        this.lastQueryUrl = p_queryUrl;
        this.lastMethod = p_method;
        this.lastUserCallback = p_userCallback;

        //will need referrence to the scope of ImgurImageSearch for this.start
        var self = this;

        //ajax success function and interception
        function jsonSuccess(p_jsonData) {
            var _jsonObject = null;
            try {
             _jsonObject = JSON.parse(p_jsonData);
            } catch (err) { console.log("json didn't parse: " + err); }

            //borrow the json to extract next page number
            self.storeNextStartIndex(_jsonObject);

            //value saved, now call user's actual passed function
            p_userCallback(_jsonObject);

            //for testing
            self.lastValidResponseQueryUrl = p_queryUrl;
        }

        //ajax failure =(
        function jsonFail(p_failText) {
            console.log("JSON failed =(  ..Did you run out of daily queries?" + p_failText);
        }

        if (settings.fakeData) {
             jsonSuccess(JSON.stringify(mockImgurImageResponse()));

        } else {
            //custom ajax request function with optional success/failure function
            var _requestHeader = null;
            if(!settings.imgur.requestHeader){
                console.log("I need your authorization header! at settings.imgur.requestHeader");
            }
            else{
                _requestHeader = settings.imgur.requestHeader;
            }
            ajax.request(p_method, p_queryUrl, jsonSuccess, jsonFail, null, _requestHeader);
        }
    };

    //forms the query and used the passed GET/METHOD with a success callback
    this.query = function(p_method, p_query, p_userCallback) {
        //check if queried before, if not do same with previous?
        this.init();
        this.sendQuery(p_method, this.getQuery(p_query), p_userCallback);
    };
    //forms the query and used the passed GET/METHOD with a success callback
    this.queryNext = function() {
        if (!this.lastMethod || !this.lastQuery || !this.lastUserCallback) {
            console.log("have to query first!");
            this.init();
            return;
        }
        this.sendQuery(this.lastMethod, this.getQuery(this.lastQuery), this.lastUserCallback);

    };
}

//Tell ImgurImageSearch to Inherit from Imgur prototype
ImgurImageSearch.prototype = Object.create(Imgur.prototype); // See note below

// Set the "constructor" property to refer to ImgurImageSearch
ImgurImageSearch.prototype.constructor = ImgurImageSearch;

