function mockGoogleImageResponse(){
    return {
        "kind": "customsearch#search",
        "url": {
            "type": "application/json",
            "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&cref={cref?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
        },
        "queries": {
            "nextPage": [{
                "title": "Google Custom Search - dog",
                "totalResults": "1590000000",
                "searchTerms": "dog",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "017150484625148396984:u7dyopvvmpe",
                "searchType": "image"
            }],
            "request": [{
                "title": "Google Custom Search - dog",
                "totalResults": "1590000000",
                "searchTerms": "dog",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "017150484625148396984:u7dyopvvmpe",
                "searchType": "image"
            }]
        },
        "context": {
            "title": "slacktastic"
        },
        "searchInformation": {
            "searchTime": 0.218354,
            "formattedSearchTime": "0.22",
            "totalResults": "1590000000",
            "formattedTotalResults": "1,590,000,000"
        },
        "items": [{
            "kind": "customsearch#result",
            "title": "Dogs | Animal Planet",
            "htmlTitle": "\u003cb\u003eDogs\u003c/b\u003e | Animal Planet",
            "link": "https://bad",
            "displayLink": "www.animalplanet.com",
            "snippet": "Dogs",
            "htmlSnippet": "\u003cb\u003eDogs\u003c/b\u003e",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "http://www.animalplanet.com/pets/dogs/",
                "height": 830,
                "width": 830,
                "byteSize": 79169,
                "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQsqEcjRUDDLwY4gTQJLZzQKUvApKIcmf8fcVhE9UxyAvKI4ZX6jQarRvVtPw",
                "thumbnailHeight": 144,
                "thumbnailWidth": 144
            }
        }, {
            "kind": "customsearch#result",
            "title": "Dog Day Care in Marda Loop Calgary - Marda Loop Doggie Daycare",
            "htmlTitle": "\u003cb\u003eDog\u003c/b\u003e Day Care in Marda Loop Calgary - Marda Loop Doggie Daycare",
            "link": "http://mardaloopdoggiedaycare.com/wp-content/uploads/2014/12/cute-dog2.jp",
            "displayLink": "mardaloopdoggiedaycare.com",
            "snippet": "cute dog2",
            "htmlSnippet": "cute dog2",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "http://mardaloopdoggiedaycare.com/",
                "height": 1080,
                "width": 1920,
                "byteSize": 553692,
                "thumbnailLink": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRfaIUByS8-6ljX0sLptMgWxunWbCdolFvIldee7etCrDMoSVchMhiltjBs",
                "thumbnailHeight": 84,
                "thumbnailWidth": 150
            }
        }, {
            "kind": "customsearch#result",
            "title": "Dog: Dog Breeds, Adoption, Bringing a Dog Home and Care",
            "htmlTitle": "\u003cb\u003eDog\u003c/b\u003e: \u003cb\u003eDog\u003c/b\u003e Breeds, Adoption, Bringing a \u003cb\u003eDog\u003c/b\u003e Home and Care",
            "link": "https://www.petfinder.com/wp-content/uploads/2012/11/200454698-001-preparing-home-new-dog-632x475-632x353.jpg",
            "displayLink": "www.petfinder.com",
            "snippet": "Bringing A Dog Home",
            "htmlSnippet": "Bringing A \u003cb\u003eDog\u003c/b\u003e Home",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://www.petfinder.com/dogs/",
                "height": 353,
                "width": 632,
                "byteSize": 39123,
                "thumbnailLink": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMZ5-rztklwPS0Jjoh6iiUF95QgarPCe3zfkBQkvcTxQ52SZ4fj5xCxQM",
                "thumbnailHeight": 77,
                "thumbnailWidth": 137
            }
        }, {
            "kind": "customsearch#result",
            "title": "Amazon.com: Dogs: Pet Supplies: Collars, Harnesses & Leashes ...",
            "htmlTitle": "Amazon.com: \u003cb\u003eDogs\u003c/b\u003e: Pet Supplies: Collars, Harnesses &amp; Leashes \u003cb\u003e...\u003c/b\u003e",
            "link": "http://g-ecx.images-amazon.com/images/G/01/img15/pet-products/small-tiles/23695_pets_vertical_store_dogs_small_tile_8._CB312176604_.jpg",
            "displayLink": "www.amazon.com",
            "snippet": "Dog Toys",
            "htmlSnippet": "\u003cb\u003eDog\u003c/b\u003e Toys",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "http://www.amazon.com/dogs-supplies-training-beds-collars-grooming/b?ie=UTF8&node=2975312011",
                "height": 305,
                "width": 593,
                "byteSize": 56996,
                "thumbnailLink": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRc5-7t_dQLu7lAze52yKrzr6sS0QxJN2ulqaZkIbSezu8h0O5ifl7Cmes",
                "thumbnailHeight": 69,
                "thumbnailWidth": 135
            }
        }, {
            "kind": "customsearch#result",
            "title": "What Kind of Dog is Right for You? - Petfinder",
            "htmlTitle": "What Kind of \u003cb\u003eDog\u003c/b\u003e is Right for You? - Petfinder",
            "link": "https://www.petfinder.com/wp-content/uploads/2012/11/dog-how-to-select-your-new-best-friend-thinkstock99062463.jpg",
            "displayLink": "www.petfinder.com",
            "snippet": "How to Select Your New Best",
            "htmlSnippet": "How to Select Your New Best",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://www.petfinder.com/pet-adoption/dog-adoption/type-dog-adoption/",
                "height": 1536,
                "width": 2048,
                "byteSize": 1790149,
                "thumbnailLink": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSqGZcxLYGBgK_IbVRU0UF5t0q7EJmQ-d89zcElAQiSlEXnWjb0TG6hLlwK",
                "thumbnailHeight": 113,
                "thumbnailWidth": 150
            }
        }, {
            "kind": "customsearch#result",
            "title": "Aggression | ASPCA",
            "htmlTitle": "Aggression | ASPCA",
            "link": "https://www.aspca.org/sites/default/files/dog-care_dog-bite-prevention_main-image.jpg",
            "displayLink": "www.aspca.org",
            "snippet": "Aggression",
            "htmlSnippet": "Aggression",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://www.aspca.org/pet-care/dog-care/common-dog-behavior-issues/aggression",
                "height": 500,
                "width": 1040,
                "byteSize": 302180,
                "thumbnailLink": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQWtLzYuahUW52YHhlKPb65iDRH3rzmpmsLDe0UB80LvSPPxVTlSGr4X36t",
                "thumbnailHeight": 72,
                "thumbnailWidth": 150
            }
        }, {
            "kind": "customsearch#result",
            "title": "Dog: Dog Breeds, Adoption, Bringing a Dog Home and Care",
            "htmlTitle": "\u003cb\u003eDog\u003c/b\u003e: \u003cb\u003eDog\u003c/b\u003e Breeds, Adoption, Bringing a \u003cb\u003eDog\u003c/b\u003e Home and Care",
            "link": "https://www.petfinder.com/wp-content/uploads/2012/11/102107675-dog-park-bullies-632x475-632x353.jpg",
            "displayLink": "www.petfinder.com",
            "snippet": "Dog-Park Bullies",
            "htmlSnippet": "\u003cb\u003eDog\u003c/b\u003e-Park Bullies",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://www.petfinder.com/dogs/",
                "height": 353,
                "width": 632,
                "byteSize": 77531,
                "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSYMjobExMo934JWsJeoED50JI92nFxu36ce_E38WJrIhDUgbmVBcZlpok",
                "thumbnailHeight": 77,
                "thumbnailWidth": 137
            }
        }, {
            "kind": "customsearch#result",
            "title": "Dog - Wikipedia, the free encyclopedia",
            "htmlTitle": "\u003cb\u003eDog\u003c/b\u003e - Wikipedia, the free encyclopedia",
            "link": "https://upload.wikimedia.org/wikipedia/commons/d/d9/Collage_of_Nine_Dogs.jpg",
            "displayLink": "en.wikipedia.org",
            "snippet": "Collage of Nine Dogs.jpg",
            "htmlSnippet": "Collage of Nine \u003cb\u003eDogs\u003c/b\u003e.jpg",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://en.wikipedia.org/wiki/Dog",
                "height": 1463,
                "width": 1665,
                "byteSize": 2286651,
                "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQOJ4hkMpwmOM_ytZr9qJxSMQVFCROt6tt0hGuZ3R0VoIlPwZptvdtqWPZ2vQ",
                "thumbnailHeight": 132,
                "thumbnailWidth": 150
            }
        }, {
            "kind": "customsearch#result",
            "title": "dog-01.jpg",
            "htmlTitle": "\u003cb\u003edog\u003c/b\u003e-01.jpg",
            "link": "http://weknownyourdreamz.com/images/dog/dog-01.jpg",
            "displayLink": "weknownyourdreamz.com",
            "snippet": "When You Dream About Dogs,",
            "htmlSnippet": "When You Dream About \u003cb\u003eDogs\u003c/b\u003e,",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "http://weknownyourdreamz.com/dog.html",
                "height": 1958,
                "width": 2510,
                "byteSize": 3127687,
                "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcS6gRVgwc3vA34oIhwSB-78umjk1FX3mXsJopzNSvzPrNl8AQm0o-uMIX8",
                "thumbnailHeight": 117,
                "thumbnailWidth": 150
            }
        }, {
            "kind": "customsearch#result",
            "title": "Dog - Wikipedia, the free encyclopedia",
            "htmlTitle": "\u003cb\u003eDog\u003c/b\u003e - Wikipedia, the free encyclopedia",
            "link": "https://upload.wikimedia.org/wikipedia/commons/e/ec/Terrier_mixed-breed_dog.jpg",
            "displayLink": "en.wikipedia.org",
            "snippet": "Mixed-breed dogs have been",
            "htmlSnippet": "Mixed-breed \u003cb\u003edogs\u003c/b\u003e have been",
            "mime": "image/jpeg",
            "image": {
                "contextLink": "https://en.wikipedia.org/wiki/Dog",
                "height": 1145,
                "width": 1280,
                "byteSize": 243497,
                "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR3HvV_qy-mtA4hqVAobUmYg6onZTFMfya1NKmpkeUL6-fiyZ2sDxu4AgJx",
                "thumbnailHeight": 134,
                "thumbnailWidth": 150
            }
        }]
    }
}