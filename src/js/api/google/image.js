'use strict';
/*global console, settings, ajax, mockGoogleImageResponse*/
// Define the Google constructor
var Google = function(p_options) {
    if (this.constructor === Google) {
        if (p_options && (typeof p_options === 'object')) {
            this.add(p_options);
        } else {
            this.add({});
        }
    } else {
        if (p_options) {
            for (var attrname in Google.prototype.options) { p_options[attrname] = Google.prototype.options[attrname]; }
        }
    }

    this.searchId = "google";
};

//Add global options to Google prototype if global options are found 'settings.google.options'
Google.prototype.add = function(p_options) {
    Google.prototype.options = p_options;
};

//Google.prototype.options = settings.google.options || {};
Google.prototype.constructor = Google;


// Define the GoogleImageSearch constructor (inherits options from Google)
function GoogleImageSearch(options) {
    //this.options = options || {};
    // Call the parent constructor, making sure (using Function#call)
    // that "this" is set correctly during the call

    //store the passed options
    if (options) {
        this.options = options;
    }

    //Call the parent constructor with passed in options
    Google.call(this, options);

    //this.options here --v  now have been modified by Google ^

    this.options = this.options || {};

    //this.options should be ammended / merged with inherited global options


    //override options passed in with Google global options

    //Following properties specific to Google Image Search API
    //override the name inherited from the generic "google"
    this.searchId = "googleImage";

    // Image option will need to be set for this specific Google Image API query string
    this.options.searchType = "image";

    this.options.start = 1;
    this.lastMethod = "";
    this.lastQuery = "";
    this.lastUserCallback = "";
    this.lastQueryUrl = "";
    this.lastValidResponseQueryUrl = "";

    //initializes the function back to its starting state
    this.init = function() {
        this.options.start = 1;
        this.lastMethod = "";
        this.lastQuery = "";
        this.lastUserCallback = "";
        this.lastQueryUrl = "";
        this.lastValidResponseQueryUrl = "";
    };

    //this returns the actual query string that we will use to send
    this.getQuery = function(p_query) {
        if (!this.options.searchType || !this.options.cx || !this.options.key || !p_query) {
            //console.log("need propery query params! DEBUG: searchType: " + this.options.searchType + " cx: " + this.options.cx + " key: " + this.options.key + " query: " + p_query + "");
            this.init();
            return null;
        }
        var queryUrl = 'https://www.googleapis.com/customsearch/v1?';
        this.options.q = encodeURI(p_query);
        for (var i in this.options) {
            queryUrl += '&' + i + '=' + this.options[i] + '';
        }

        this.lastQuery = p_query;
        return queryUrl;
    };

    //This function extracts the start point for queryNext()..i.e. image 11 of results
    this.storeNextStartIndex = function(p_jsonObject) {
        //borrow the json to extract next page number
        try {
            //console.log(p_jsonObject)
            if(p_jsonObject.queries.nextPage[0].startIndex > this.options.start)
            {
                this.options.start = p_jsonObject.queries.nextPage[0].startIndex
            }
        } catch (err) {
            console.log("error with decode" + err);
        }

    };

    //This does much of the heavy lifting, sends off the ajax request and parses the results
    //send to the user's callback function for processing the result object
    this.sendQuery = function(p_method, p_queryUrl, p_userCallback) {
        if (!p_method || !p_queryUrl || !p_userCallback) {
            //console.log("need proper params: p_method: " + p_method + " p_queryUrl");
            return null;
        }

        this.lastQueryUrl = p_queryUrl;
        this.lastMethod = p_method;
        this.lastUserCallback = p_userCallback;

        //will need referrence to the scope of GoogleImageSearch for this.options.start
        var self = this;

        //ajax success function and interception
        function jsonSuccess(p_jsonData) {
            var _jsonObject = null;
            try {
             _jsonObject = JSON.parse(p_jsonData);
            } catch (err) { console.log("json didn't parse: " + err); }

            //borrow the json to extract next page number
            self.storeNextStartIndex(_jsonObject);

            //value saved, now call user's actual passed function
            p_userCallback(_jsonObject);

            //for testing
            self.lastValidResponseQueryUrl = p_queryUrl;
        }

        //ajax failure =(
        function jsonFail(p_failText) {
            console.log("JSON failed =(  ..Did you run out of daily queries?" + p_failText);
        }
        if (settings.fakeData) {
             jsonSuccess(JSON.stringify(mockGoogleImageResponse()));

        } else {
            //custom ajax request function with optional success/failure function
            ajax.request(p_method, p_queryUrl, jsonSuccess, jsonFail);
        }
    };

    //forms the query and used the passed GET/METHOD with a success callback
    this.query = function(p_method, p_query, p_userCallback) {
        //check if queried before, if not do same with previous?
        this.init();
        this.sendQuery(p_method, this.getQuery(p_query), p_userCallback);
    };
    //forms the query and used the passed GET/METHOD with a success callback
    this.queryNext = function() {
        if (!this.lastMethod || !this.lastQuery || !this.lastUserCallback) {
            console.log("have to query first!");
            this.init();
            return;
        }
        this.sendQuery(this.lastMethod, this.getQuery(this.lastQuery), this.lastUserCallback);

    };
}

//Tell GoogleImageSearch to Inherit from Google prototype
GoogleImageSearch.prototype = Object.create(Google.prototype); // See note below

// Set the "constructor" property to refer to GoogleImageSearch
GoogleImageSearch.prototype.constructor = GoogleImageSearch;