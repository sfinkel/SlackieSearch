"use strict";
/*global console, settings, ajax, mockFlikrImageResponse*/

// Define the Flikr constructor
var Flikr = function(p_options) {
    if (this.constructor === Flikr) {
        if (p_options && (typeof p_options === 'object')) {
            this.add(p_options);
        } else {
            this.add({});
        }
    } else {
        if (p_options) {
            for (var attrname in Flikr.prototype.options) { p_options[attrname] = Flikr.prototype.options[attrname]; }
        }
    }

    this.searchId = "flikr";
};

//Add global options to Flikr prototype if global options are found 'settings.flikr.options'
Flikr.prototype.add = function(p_options) {
    //console.log(p_options);
    Flikr.prototype.options = p_options;
};

//Flikr.prototype.options = settings.flikr.options || {};
Flikr.prototype.constructor = Flikr;


// Define the FlikrImageSearch constructor (inherits options from Flikr)
function FlikrImageSearch(options) {
    //this.options = options || {};
    // Call the parent constructor, making sure (using Function#call)
    // that "this" is set correctly during the call

    //store the passed options
    if (options) {
        this.options = options;
    }

    //Call the parent constructor with passed in options
    Flikr.call(this, options);

    //this.options here --v  now have been modified by Flikr ^

    this.options = this.options || {};

    //this.options should be ammended / merged with inherited global options


    //override options passed in with Flikr global options

    //Following properties specific to Flikr Image Search API
    //override the name inherited from the generic "flikr"
    this.searchId = "flikrImage";

    this.options.page = 1;
    this.options.format = "json";
    this.options.nojsoncallback = "1";

    this.lastMethod = "";
    this.lastQuery = "";
    this.lastUserCallback = "";
    this.lastQueryUrl = "";
    this.lastValidResponseQueryUrl = "";

    //initializes the function back to its starting state
    this.init = function() {
        this.options.page = 1;
        this.options.format = "json";
        this.options.nojsoncallback = "1";

        this.lastMethod = "";
        this.lastQuery = "";
        this.lastUserCallback = "";
        this.lastQueryUrl = "";
        this.lastValidResponseQueryUrl = "";
    };

    //this returns the actual query string that we will use to send
    this.getQuery = function(p_query) {
        if (!this.options.method || !this.options.api_key || !this.options.format || !p_query) {
            //console.log("need propery query params! DEBUG: method: " + this.options.method + " api_key: " + this.options.api_key + " text: " + p_query + "");
            this.init();
            return null;
        }

        var queryUrl = 'https://api.flickr.com/services/rest/?';
        // 'text=dogs' ...we use text for search, dogs as query for search
        this.options.text = encodeURI(p_query);

        for (var i in this.options) {
            queryUrl += '&' + i + '=' + this.options[i] + '';
        }

        this.lastQuery = p_query;
        return queryUrl;
    };

    //This function extracts the start point for queryNext()..i.e. page 2 of results
    this.storeNextStartIndex = function(p_jsonObject) {
        //borrow the json to extract next page number
        try {
            //console.log(p_jsonObject)
            if (p_jsonObject.photos.page++ < p_jsonObject.photos.pages)
                this.options.page = p_jsonObject.photos.page++;

        } catch (err) {
            console.log("error with decode" + err);
        }

    };

    //This does much of the heavy lifting, sends off the ajax request and parses the results
    //send to the user's callback function for processing the result object
    this.sendQuery = function(p_method, p_queryUrl, p_userCallback) {
        if (!p_method || !p_queryUrl || !p_userCallback) {
            console.log("need proper params: p_method: " + p_method + " p_queryUrl");
            return null;
        }

        this.lastQueryUrl = p_queryUrl;
        this.lastMethod = p_method;

        //save the user's callback in this scope
        //want to intercept data before exec'ing user's function
        this.lastUserCallback = p_userCallback;

        //will need referrence to the scope of FlikrImageSearch for this.options.start
        var self = this;

        //ajax success function and interception
        function jsonSuccess(p_jsonData) {
            //console.log(p_jsonData)
            var _jsonObject = null;
            try {
                _jsonObject = JSON.parse(p_jsonData);
            } catch (err) { console.log("json didn't parse: " + err); }

            //borrow the json to extract next page number
            self.storeNextStartIndex(_jsonObject);

            //value saved, now call user's actual passed function
            p_userCallback(_jsonObject);

            //for testing
            self.lastValidResponseQueryUrl = p_queryUrl;
        }

        //ajax failure =(
        function jsonFail(p_failText) {
            console.log("JSON failed =(" + p_failText);
        }

        if (settings.fakeData) {
            jsonSuccess(JSON.stringify(mockFlikrImageResponse()));
        } else {
            //custom ajax request function with optional success/failure function
            ajax.request(p_method, p_queryUrl, jsonSuccess, jsonFail);
        }


    };

    //forms the query and used the passed GET/METHOD with a success callback
    this.query = function(p_method, p_query, p_userCallback) {
        //check if queried before, if not do same with previous?
        this.init();
        this.sendQuery(p_method, this.getQuery(p_query), p_userCallback);
    };
    //forms the query and used the passed GET/METHOD with a success callback
    this.queryNext = function() {
        if (!this.lastMethod || !this.lastQuery || !this.lastUserCallback) {
            console.log("have to query first!");
            this.init();
            return;
        }
        this.sendQuery(this.lastMethod, this.getQuery(this.lastQuery), this.lastUserCallback);

    };
}

//Tell FlikrImageSearch to Inherit from Flikr prototype
FlikrImageSearch.prototype = Object.create(Flikr.prototype); // See note below

// Set the "constructor" property to refer to FlikrImageSearch
FlikrImageSearch.prototype.constructor = FlikrImageSearch;