 function mockFlikrImageResponse() {
     return {
         "photos": {
             "page": 1,
             "pages": "36438",
             "perpage": 10,
             "total": "364378",
             "photo": [
                 { "id": "26081922810", "owner": "13508814@N06", "secret": "f769d7f6e8", "server": "1649", "farm": 2, "title": "I Am Merciful", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "25749934424", "owner": "53017973@N07", "secret": "598ed5d41d", "server": "1478", "farm": 2, "title": "Basilica De La Sagrada Familia", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26328833346", "owner": "53017973@N07", "secret": "84c967a6d9", "server": "1538", "farm": 2, "title": "Basilica De La Sagrada Familia", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "25752028633", "owner": "137395192@N08", "secret": "7759894546", "server": "1679", "farm": 2, "title": "Faraday, Napuaa and Guass all sat pretty for the camera.", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "25752027923", "owner": "53017973@N07", "secret": "c8f8d04236", "server": "1576", "farm": 2, "title": "Basilica De La Sagrada Familia", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26328826056", "owner": "53017973@N07", "secret": "2b0af37392", "server": "1712", "farm": 2, "title": "Barcelona_4683.jpg", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26328820006", "owner": "53017973@N07", "secret": "46ef9d0f06", "server": "1574", "farm": 2, "title": "Apartment Building", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26328814076", "owner": "94765126@N06", "secret": "a954a2a8f7", "server": "1509", "farm": 2, "title": "", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26288530371", "owner": "16355274@N04", "secret": "df1714ebd6", "server": "1549", "farm": 2, "title": "P3150024.jpg", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
                 { "id": "26288528931", "owner": "53017973@N07", "secret": "4dfc24cc8c", "server": "1708", "farm": 2, "title": "Barcelona_4663.jpg", "ispublic": 1, "isfriend": 0, "isfamily": 0 }
             ]
         },
         "stat": "ok"
     }
 }
