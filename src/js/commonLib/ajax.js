var ajax = function() {
    /**
     * Make a X-Domain request to url and p_callback.
     *
     * @param url {String}
     * @param method {String} HTTP verb ('GET', 'POST', 'DELETE', etc.)
     * @param data {String} request body
     * @param p_callback {Function} to call on success
     * @param errback {Function} to call on error
     */
    var xhr = function(method, url, p_callback, errback, data, p_requestHeaders) {
        var req;

        if (XMLHttpRequest) {
            req = new XMLHttpRequest();
            req.open(method, url, true);
            if (0 && 'withCredentials' in req) {
                if (errback) {
                    req.onerror = errback;
                }
                req.onload = p_callback;
                req.onerror = errback;
            } else {
                req.onreadystatechange = function() {
                    if (req.readyState === 4) {
                        if (req.status >= 200 && req.status < 400) {
                            p_callback(req.responseText);
                        } else {
                            if (errback) {
                                errback();
                            } else {
                                console.log("onreadystatechange error req.status");
                            }
                        }
                    }
                };
            }
        } else if (XDomainRequest) {
            req = new XDomainRequest();
            req.open(method, url);
            req.onerror = errback;
            req.onload = function() {
                p_callback(req.responseText);
            };
        } else {
            if (errback) {
                errback();
            } else {
                console.log('bad ajax not supported')
            }

        }
        for (key in p_requestHeaders) {
            req.setRequestHeader(key, p_requestHeaders[key]);
        }

        req.send(data);
    }

    return {
        request: xhr
    }

}()