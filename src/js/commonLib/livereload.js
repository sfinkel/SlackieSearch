if (settings.liveReload) {
    console.log("Debug mode is: true - Trying to enable livereload");

    document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
        ':3000/livereload.js?snipver=1"></' + 'script>');
}